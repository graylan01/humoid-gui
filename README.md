
## Humoid-GUI

### Project Overview

The `humoid-gui` project is a graphical user interface (GUI) application leveraging the Llama-2 model for generating engaging text-based stories. It includes essential natural language processing dependencies and dynamically downloads the Llama-2 model at runtime to keep the Docker image size minimal.

# Install with Docker on Linux/Ubuntu 24.04
### How to Pull and Run the Docker Image


# Install docker 
Requires Ubuntu 24.04 and Docker 24.0.5
```
sudo snap install docker
```

# Dockerhub repo

 Find the dockerhub repo here.

 ```
 https://hub.docker.com/r/graylan01/humoid-gui
 ```
# Install with dockerhub option
 
1. **Pull the Docker Image**:

   Pull the latest Docker image from Docker Hub using the following command:

   ```
   docker pull graylan01/humoid-gui:latest 
   ```

2. **Run the Docker Container**:

   Start the container and launch the GUI application with:


```
docker run --rm -it \
    --net=host \
    --env="DISPLAY" \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    --volume="$HOME/humoid_data:/data" \
    --volume="$HOME/humoid_data/nltk_data:/root/nltk_data" \
    --volume="$HOME/humoid_data/weaviate:/root/.cache/weaviate-embedded" \
    graylan01/humoid-gui:latest


```


# Demo Image

### Prompt
![Demo Screenshot](https://gitlab.com/graylan01/humoid-gui/-/raw/main/demo.png)

### With Reply
![Demo 2 Screenshot](https://gitlab.com/graylan01/humoid-gui/-/raw/main/demo2.png)

### Behind the Scenes

- **Model Download Script**:
  A script checks if the Llama-2 model file exists. If not, it downloads and verifies the model file at runtime. The same is done with NLTK and Weaviate.

 

### Docker Build Instructions

 # Linux / Ubuntu Build Instructions

Start/Build Humoid GUI using docker

### Clone Repo

```
git clone https://gitlab.com/graylan01/humoid-gui
```

## Build the Image for Dockerhub
```
docker build --no-cache -t graylan01/humoid-gui:latest .

```

# Start Docker GUI

```
docker run --rm -it \
    --net=host \
    --env="DISPLAY" \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    --volume="$HOME/humoid_data:/data" \
    --volume="$HOME/humoid_data/nltk_data:/root/nltk_data" \
    --volume="$HOME/humoid_data/weaviate:/root/.cache/weaviate-embedded" \
    graylan01/humoid-gui:latest


```

### Cool Prompts to Try

1. **Fantasy Adventure**:
   ```
   "Create a story about a young hero who discovers a magical kingdom hidden in the clouds."
   ```

2. **Sci-Fi Mystery**:
   ```
   "Write a tale of an astronaut who encounters a mysterious signal from an unknown planet."
   ```

3. **Historical Drama**:
   ```
   "Describe the life of a medieval knight who uncovers a royal conspiracy."
   ```

4. **Romantic Comedy**:
   ```
   "Tell a story about two rivals who accidentally fall in love during a cooking competition."
   ```

5. **Horror Thriller**:
   ```
   "Narrate the experience of a group of friends who explore an abandoned mansion with a dark secret."
   ```

 

## Non-Docker Installation

### Prerequisites

Before starting, make sure you have the following installed:

- Python 3.11 or higher
- pip (Python package installer)
- Git (optional, for cloning the repository)

### Clone the Repository

Clone the repository to your local machine using Git:

```bash
git clone https://gitlab.com/graylan01/humoid-gui.git
cd humoid-gui
```

### Install Dependencies

Navigate to the project directory and install the required Python packages using pip. It's recommended to do this within a virtual environment:

```bash
# Navigate to the project directory
cd humoid-gui

# Create and activate a virtual environment (optional but recommended)
python -m venv venv
# On Windows
venv\Scripts\activate
# On macOS/Linux
source venv/bin/activate

# Install dependencies
pip install -r requirements.txt
```
Once everything is set up, you can run the application:

```bash
python main.py
```

The application GUI will launch, allowing you to interact with the AI system.

## Usage

- Enter your username in the provided field and click "Update" to set your username.
- Type your message in the input textbox and press Enter or click "Send" to send it to the AI.
- The AI's responses will appear in the main text box.
- Images generated based on your input may also appear in the image display area.
- For AI generated Images appear you need to install Automatic1111 WebUI with --api varible enabled

This project is licensed under the [GNU General Public License v2.0](LICENSE).
Certainly! Enhancing the security of your Dockerfile is crucial to protect your application and its dependencies from potential vulnerabilities. Below, I provide an improved Dockerfile with detailed explanations of the security enhancements. Additionally, I'll include a recommended `entrypoint.sh` script to handle configuration securely and suggest best practices for managing secrets.

---

## Improved Dockerfile

```dockerfile
# Use an official Python runtime as a parent image
FROM python:3.11-slim-buster

# Set environment variables to ensure Python outputs are logged correctly
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    DEBIAN_FRONTEND=noninteractive \
    LANG=C.UTF-8 \
    LC_ALL=C.UTF-8

# Create a non-root user and group for running the application
RUN addgroup --system appgroup && adduser --system --ingroup appgroup appuser

# Install necessary system packages and clean up to reduce image size
RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        python3-dev \
        python3-tk \
        libgl1-mesa-glx \
        curl \
    && rm -rf /var/lib/apt/lists/*

# Set the working directory inside the container
WORKDIR /app

# Copy only requirements.txt first to leverage Docker layer caching
COPY requirements.txt .

# Install Python dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the entire current directory contents into the container at /app
COPY . .

# Create a directory for model data and set appropriate permissions
RUN mkdir -p /data && chown appuser:appgroup /data

# Ensure the download script has execute permissions
RUN chmod +x /app/download_model.sh

# Copy the entrypoint script into the container
COPY entrypoint.sh /app/entrypoint.sh
RUN chmod +x /app/entrypoint.sh

# Switch to the non-root user to run the application
USER appuser

# Expose the necessary port (adjust if different)
EXPOSE 8000

# Define a volume for persistent model data
VOLUME /data

# Set the entrypoint to the entrypoint script
ENTRYPOINT ["/app/entrypoint.sh"]

# Default command to run the application
CMD ["python", "main.py"]
```

---

## `entrypoint.sh` Script

Create a file named `entrypoint.sh` in your project directory with the following content:

```bash
#!/bin/bash
set -e

# Function to generate a secure API key if not provided
generate_api_key() {
  if [ -z "$API_KEY" ]; then
    echo "Generating a random API key..."
    export API_KEY=$(python -c "import secrets; print(secrets.token_urlsafe(32))")
  fi
}

# Generate API key if not set
generate_api_key

# Generate config.json from environment variables
if [ ! -f /app/config.json ]; then
  echo "Generating config.json from environment variables..."
  python -c "
import os, json

config = {
    'DB_NAME': os.getenv('DB_NAME', 'story_generator.db'),
    'WEAVIATE_ENDPOINT': os.getenv('WEAVIATE_ENDPOINT', 'http://localhost:8079'),
    'WEAVIATE_QUERY_PATH': os.getenv('WEAVIATE_QUERY_PATH', '/v1/graphql'),
    'LLAMA_MODEL_PATH': os.getenv('LLAMA_MODEL_PATH', '/data/llama-2-7b-chat.ggmlv3.q8_0.bin'),
    'IMAGE_GENERATION_URL': os.getenv('IMAGE_GENERATION_URL', 'http://127.0.0.1:7860/sdapi/v1/txt2img'),
    'MAX_TOKENS': int(os.getenv('MAX_TOKENS', '3999')),
    'CHUNK_SIZE': int(os.getenv('CHUNK_SIZE', '1250')),
    'API_KEY': os.getenv('API_KEY'),
    'WEAVIATE_API_URL': os.getenv('WEAVIATE_API_URL', 'http://localhost:8079/v1/objects'),
    'ELEVEN_LABS_KEY': os.getenv('ELEVEN_LABS_KEY', 'apikyhere')
}

with open('/app/config.json', 'w') as f:
    json.dump(config, f)
"
fi

# Execute the model download script
/app/download_model.sh

# Execute the main application
exec "$@"
```

**Explanation of `entrypoint.sh`:**

1. **Secure API Key Generation:**
   - Checks if the `API_KEY` environment variable is set. If not, it generates a secure random API key using Python's `secrets` module.
   - This ensures that the API key isn't hardcoded or generated during the image build, enhancing security.

2. **Configuration Handling:**
   - Generates a `config.json` file from environment variables at container startup rather than during the build process. This avoids embedding sensitive information into the Docker image.
   - Allows flexibility to override configuration parameters without modifying the image.

3. **Model Download:**
   - Runs the `download_model.sh` script to download the necessary model files if they aren't already present, ensuring that the container remains lightweight and only includes what's necessary.

4. **Executing the Main Application:**
   - Uses `exec "$@"` to run the main application (`python main.py`) as specified in the Dockerfile's `CMD`.

---

## Security Enhancements Explained

### 1. **Use a Non-Root User**

Running containers as a non-root user minimizes the risk of privilege escalation within the container. If an attacker exploits a vulnerability in your application, they won't have root access within the container.

```dockerfile
# Create a non-root user and group for running the application
RUN addgroup --system appgroup && adduser --system --ingroup appgroup appuser

# Switch to the non-root user to run the application
USER appuser
```

### 2. **Handle Secrets Securely**

**Avoid Baking Secrets into the Image:**

In your original Dockerfile, secrets like `API_KEY` and `ELEVEN_LABS_KEY` were generated and written into `config.json` during the build process. This approach embeds secrets into the image, which can be a significant security risk.

**Solution:**

- **Use Environment Variables:** Pass secrets as environment variables at runtime. This approach keeps secrets out of the image and allows for easier rotation and management.
  
- **Generate Configuration at Startup:** The `entrypoint.sh` script generates `config.json` from environment variables when the container starts, ensuring that secrets aren't stored in the image layers.

```bash
# Generate config.json from environment variables
if [ ! -f /app/config.json ]; then
  echo "Generating config.json from environment variables..."
  python -c "
import os, json

config = {
    'DB_NAME': os.getenv('DB_NAME', 'story_generator.db'),
    'WEAVIATE_ENDPOINT': os.getenv('WEAVIATE_ENDPOINT', 'http://localhost:8079'),
    'WEAVIATE_QUERY_PATH': os.getenv('WEAVIATE_QUERY_PATH', '/v1/graphql'),
    'LLAMA_MODEL_PATH': os.getenv('LLAMA_MODEL_PATH', '/data/llama-2-7b-chat.ggmlv3.q8_0.bin'),
    'IMAGE_GENERATION_URL': os.getenv('IMAGE_GENERATION_URL', 'http://127.0.0.1:7860/sdapi/v1/txt2img'),
    'MAX_TOKENS': int(os.getenv('MAX_TOKENS', '3999')),
    'CHUNK_SIZE': int(os.getenv('CHUNK_SIZE', '1250')),
    'API_KEY': os.getenv('API_KEY'),
    'WEAVIATE_API_URL': os.getenv('WEAVIATE_API_URL', 'http://localhost:8079/v1/objects'),
    'ELEVEN_LABS_KEY': os.getenv('ELEVEN_LABS_KEY', 'apikyhere')
}

with open('/app/config.json', 'w') as f:
    json.dump(config, f)
"
fi
```

**Passing Environment Variables:**

When running the container, pass the necessary environment variables securely. For example:

```bash
docker run -d \
  -e DB_NAME=story_generator.db \
  -e WEAVIATE_ENDPOINT=http://weaviate:8079 \
  -e WEAVIATE_QUERY_PATH=/v1/graphql \
  -e LLAMA_MODEL_PATH=/data/llama-2-7b-chat.ggmlv3.q8_0.bin \
  -e IMAGE_GENERATION_URL=http://image-generator:7860/sdapi/v1/txt2img \
  -e MAX_TOKENS=3999 \
  -e CHUNK_SIZE=1250 \
  -e API_KEY=your_secure_api_key \
  -e WEAVIATE_API_URL=http://weaviate:8079/v1/objects \
  -e ELEVEN_LABS_KEY=your_eleven_labs_key \
  --name your_app_container your_app_image
```

**Consider Using Docker Secrets:**

For even better security, especially in orchestrated environments like Docker Swarm or Kubernetes, consider using Docker secrets to manage sensitive information.

### 3. **Minimize Image Size and Attack Surface**

**Use `--no-install-recommends`:**

This option prevents the installation of unnecessary packages, reducing the image size and potential vulnerabilities.

```dockerfile
RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        python3-dev \
        python3-tk \
        libgl1-mesa-glx \
        curl \
    && rm -rf /var/lib/apt/lists/*
```

**Clean Up After Installation:**

Removing the apt lists after installing packages ensures that unnecessary files aren't left in the image.

```dockerfile
&& rm -rf /var/lib/apt/lists/*
```

### 4. **Use Multi-Stage Builds (Optional)**

For applications that require building assets or compiling code, multi-stage builds can help keep the final image lean. However, in your current setup, this might not be necessary unless you have specific build-time dependencies.

### 5. **Set File Permissions Appropriately**

Ensure that scripts and sensitive files have the correct permissions to prevent unauthorized access or modifications.

```dockerfile
# Ensure the download script has execute permissions
RUN chmod +x /app/download_model.sh

# Copy the entrypoint script into the container
COPY entrypoint.sh /app/entrypoint.sh
RUN chmod +x /app/entrypoint.sh
```

### 6. **Expose Only Necessary Ports**

Only expose the ports that are required for your application to function. In your case, if FastAPI is running on port 8000, expose that.

```dockerfile
EXPOSE 8000
```

### 7. **Use Volumes for Persistent Data**

Define volumes for directories that require persistent storage, such as model files. This approach prevents data loss when containers are recreated and allows for easier data management.

```dockerfile
VOLUME /data
```

### 8. **Limit Container Capabilities (Advanced)**

For enhanced security, you can limit the capabilities of the container using Docker's `--cap-drop` and `--cap-add` options. This step is more advanced and depends on your application's requirements.

---

## Additional Recommendations

### 1. **Use `.dockerignore` to Exclude Unnecessary Files**

Create a `.dockerignore` file in your project directory to prevent copying unnecessary files into the Docker image. This practice reduces the image size and prevents potential leakage of sensitive files.

**Example `.dockerignore`:**

```
__pycache__
*.pyc
*.pyo
*.pyd
.env
.git
.gitignore
Dockerfile
README.md
*.log
*.sqlite3
```

### 2. **Regularly Update Dependencies**

Ensure that both system packages and Python dependencies are regularly updated to incorporate security patches.

- **System Packages:** Use specific versions if possible or automate updates.
- **Python Dependencies:** Regularly update `requirements.txt` and rebuild the image.

### 3. **Implement Health Checks**

Define `HEALTHCHECK` in your Dockerfile to allow Docker to monitor the health of your application.

**Example Health Check:**

```dockerfile
HEALTHCHECK --interval=30s --timeout=10s --start-period=5s --retries=3 \
  CMD curl -f http://localhost:8000/health || exit 1
```

Ensure your FastAPI application has a `/health` endpoint that returns a 200 status code when healthy.

### 4. **Use Read-Only Filesystem (Optional)**

For added security, run the container with a read-only filesystem and use volumes for directories that need write access.

**Example Docker Run Command:**

```bash
docker run -d \
  --read-only \
  -v /data \
  -e API_KEY=your_secure_api_key \
  your_app_image
```

**Note:** Ensure that your application can operate correctly with a read-only filesystem.

### 5. **Monitor and Scan Images for Vulnerabilities**

Use tools like [Docker Bench for Security](https://github.com/docker/docker-bench-security) and [Trivy](https://github.com/aquasecurity/trivy) to scan your Docker images for vulnerabilities regularly.

---

## Summary of Key Changes

1. **Non-Root User:** The container runs as a non-root user (`appuser`) to minimize security risks.
2. **Secure Configuration Handling:** `config.json` is generated at container startup using environment variables, preventing secrets from being baked into the image.
3. **Reduced Attack Surface:** Installed only necessary system packages with `--no-install-recommends` and cleaned up apt lists to reduce image size and potential vulnerabilities.
4. **Entrypoint Script:** Centralizes the setup process, including configuration generation and model downloading, enhancing maintainability and security.
5. **Environment Variables for Secrets:** Encourages the use of environment variables for sensitive information, aligning with best practices for secret management.
6. **Volume Definition:** Defines a volume for persistent data (`/data`), ensuring data persists across container restarts and isolating mutable data from the application code.

--- 
### 1. **Build the Docker Image**

```bash
docker build -t your_app_image .
```

### 2. **Run the Docker Container with Environment Variables**

```bash
docker run -d \
  -e DB_NAME=story_generator.db \
  -e WEAVIATE_ENDPOINT=http://weaviate:8079 \
  -e WEAVIATE_QUERY_PATH=/v1/graphql \
  -e LLAMA_MODEL_PATH=/data/llama-2-7b-chat.ggmlv3.q8_0.bin \
  -e IMAGE_GENERATION_URL=http://127.0.0.1:7860/sdapi/v1/txt2img \
  -e MAX_TOKENS=3999 \
  -e CHUNK_SIZE=1250 \
  -e API_KEY=$(openssl rand -base64 32) \
  -e WEAVIATE_API_URL=http://weaviate:8079/v1/objects \
  -e ELEVEN_LABS_KEY=your_eleven_labs_key \
  --name your_app_container \
  --network your_network \
  -p 8000:8000 \
  your_app_image
```

**Notes:**

- **Environment Variables:** Replace placeholders like `your_eleven_labs_key` with your actual secrets. Consider using a secure method to inject these variables, such as Docker secrets or a secrets manager.
- **Networking:** Ensure the container is attached to the appropriate Docker network to communicate with services like Weaviate and the image generation service.
- **Ports:** Adjust port mappings (`-p 8000:8000`) based on your application's requirements.

### 3. **Using Docker Compose (Optional)**

For more complex setups, consider using Docker Compose to manage environment variables, volumes, and networking.

**Example `docker-compose.yml`:**

```yaml
version: '3.8'

services:
  app:
    build: .
    container_name: your_app_container
    environment:
      DB_NAME: story_generator.db
      WEAVIATE_ENDPOINT: http://weaviate:8079
      WEAVIATE_QUERY_PATH: /v1/graphql
      LLAMA_MODEL_PATH: /data/llama-2-7b-chat.ggmlv3.q8_0.bin
      IMAGE_GENERATION_URL: http://image-generator:7860/sdapi/v1/txt2img
      MAX_TOKENS: 3999
      CHUNK_SIZE: 1250
      API_KEY: ${API_KEY}
      WEAVIATE_API_URL: http://weaviate:8079/v1/objects
      ELEVEN_LABS_KEY: ${ELEVEN_LABS_KEY}
    ports:
      - "8000:8000"
    volumes:
      - data_volume:/data
    networks:
      - app_network

  weaviate:
    image: semitechnologies/weaviate:latest
    ports:
      - "8079:8079"
    environment:
      QUERY_DEFAULTS: '{ "certs": { "secure": false } }'
    networks:
      - app_network

  image-generator:
    image: your_image_generator_image
    ports:
      - "7860:7860"
    networks:
      - app_network

volumes:
  data_volume:

networks:
  app_network:
    driver: bridge
```

**`.env` File:**

Create a `.env` file to store your secrets securely.

```
API_KEY=your_secure_api_key
ELEVEN_LABS_KEY=your_eleven_labs_key
```

**Run Docker Compose:**

```bash
docker-compose up -d
```

**Advantages of Using Docker Compose:**

- **Simplified Configuration:** Manage multiple services, environment variables, and volumes in a single file.
- **Secret Management:** Leverage Docker Compose's support for environment variables and external secrets.
- **Networking:** Automatically sets up a Docker network for inter-service communication.

 